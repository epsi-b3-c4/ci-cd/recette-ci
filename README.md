# Projet de RAYNAL Julien B3 C4

### Commandes pour les docker
Création du réseau:<br>
`docker network create --driver bridge dnetwork
`<br>
Création de la bdd:<br>`docker run -d --name mysql_container -p3306:3306 --net=dnetwork  registry.gitlab.com/epsi-b3-c4/ci-cd/recette-ci:database
`<br>
Création du serveur spring:<br>
`docker run -d --name spring_container -p8080:8080 --net=dnetwork registry.gitlab.com/epsi-b3-c4/ci-cd/recette-ci:spring
`
<br>
#### URL jdbc: `jdbc:mysql://mysql_container:3306/recette`
<br>

#### page web: `http://localhost:8080/`
<br>

Toute l'application est en fait un serveur spring sans aucune page html.<br>
Toute la partie front que vous voyez est généré uniquement par des retours API et le code.<br>
C'était pour essayer de me séparer des framework SPA vu qu'il s'agit d'un à focus projet CI/CD.<br>
<br>
Bonne journée monsieur
