CREATE DATABASE IF NOT EXISTS recette;

USE recette;

create table IF NOT EXISTS Recette (
    Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Nom varchar(45),
    Description varchar(256)
) ENGINE=InnoDB;

create table IF NOT EXISTS Ingredient (
    Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Nom varchar(45)
) ENGINE=InnoDB;

create table IF NOT EXISTS Quantite (
    IdRecette int,
    IdIngredient int,
    quantite int,
    MeasureUnit varchar(5),
    FOREIGN KEY (IdRecette) REFERENCES Recette(Id),
    FOREIGN KEY (IdIngredient) REFERENCES Ingredient(Id)
) ENGINE=InnoDB;

INSERT INTO Recette (Nom, Description) VALUE ('Choucroute', 'Oscur');
INSERT INTO Recette (Nom, Description) VALUE ('Couscous', 'gros couscous');
INSERT INTO Recette (Nom, Description) VALUE ('Bretzel', 'du pain et du sel');

INSERT INTO Ingredient (Nom)  VALUES ('tomate'), ('haricot');
INSERT INTO Quantite (IdRecette, IdIngredient, quantite, MeasureUnit) VALUE (1, 1, 8, 'kg');
INSERT INTO Quantite (IdRecette, IdIngredient, quantite, MeasureUnit) VALUE (1, 2, 20, 'l');
