package com.raynal.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class Controller {
//    @GetMapping("/")
//    public String welcomePage(Model model) {
//        model.addAttribute("date", new Date().toString());
//
//        // Return the view
//        return "welcome";
//    }

    public static String title =  "<h1>RECETTES</h1><p>Tous les résultats sont dans la section résultat en bas de la page</p>";
    public static String getAllRecipe = "<br><br><br><button onclick=\"location.href='http://127.0.0.1:8080/all';\" id='allRecipe'>Toutes les recettes en bdd</button>";
    public static String createRecipe = "<br><br><br><h3>Créé recette</h3><br><form action='http://127.0.0.1:8080/create' method='POST'>" +
            "<div><label for='nom'>Nom de la recette</label><input name='name' id='name' value='recette'></div>" +
            "<div><label for='description'>Description de la recette</label><input name='description' id='description' value='une recette ...'></div>" +
            "<div><button>Submit the recipe</button></div></form>";
    public static String detailRecipe = "<br><br><br><h3>Détails recette(s)/mise à jour</h3><br><form action='http://127.0.0.1:8080/detail' method='POST'>" +
            "<div><label for='nom'>Nom des/de la recette recherché</label><input name='name' id='name' value='recette'></div>" +
            "<div><button>Trouvé la recette</button></div></form>";
    public static String deleteRecipe =  "<br><br><br><h3>Supprimer recette</h3><br><form action='http://127.0.0.1:8080/deleterecipe' method='POST'>" +
            "<div><label for='nom'>Id de la recette</label><input name='id' id='id' value='id recette'></div>" +
            "<div><button>supprimer la recette</button></div></form>";
    public static String addIngredient = "<br><br><br><h3>Ajouter ingrédient</h3><br><form action='http://127.0.0.1:8080/addingredient' method='POST'>" +
            "<div><label for='nom'>Id de la recette</label><input name='id' id='id' value='id recette'></div>" +
            "<div><label for='nom'>Nom de l'ingredient</label><input name='name' id='name' value='ingredient'></div>" +
            "<div><label for='quantity'>Quantité de l'ingrédient</label><input name='quantity' id='quantity' value='quantity'></div>" +
            "<div><label for='unit'>Unité de mesure</label><input name='unit' id='unit' value='unit'></div>" +
            "<div><button>Submit the recipe</button></div></form>";
    public static String deleteIngredient = "<br><br><br><h3>Supprimer ingredient de recette</h3><br><form action='http://127.0.0.1:8080/deleteingredient' method='POST'>" +
            "<div><label for='idr'>Id de la recette</label><input name='idr' id='idr' value='id recette'></div>" +
            "<div><label for='idi'>Id de l'ingredient</label><input name='idi' id='idi' value='id ingredient'></div>" +
            "<div><button>supprimer l'ingredient</button></div></form>";
    public static String resultat =  "<h2>Résultat:</h2>";

    public static String basicPage = title + getAllRecipe + createRecipe + detailRecipe + deleteRecipe + addIngredient + deleteIngredient;

    @GetMapping("/")
    public static String recipeControl() {
        return basicPage;
    }

    @GetMapping("/all")
    public static String recipes() {
        List<Recipe> list = Recipe.get_all_recipe();
        StringBuilder html = new StringBuilder("<table border='2'><thead><tr><th>id</th><th>nom</th><th>description</th></tr></thead><tbdody>");
        for (Recipe recipe: list) {
            html.append(String.format("<tr><td>%d</td><td>%s</td><td>%s</td></tr>", recipe.getId(), recipe.getName(), recipe.getDescription()));
        }
        html.append("</tbody></table>");
        return title + getAllRecipe + resultat + html + createRecipe +  detailRecipe + deleteRecipe + addIngredient + deleteIngredient;
    }

    @PostMapping("/create")
    public static String createRecipe(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        String name = params.get("name")[0];
        String description = params.get("description")[0];
        Recipe.createRecipe(name, description);
        return title + getAllRecipe + createRecipe + resultat + String.format("<h2>La recette: %s a bien été inseré dans la base de donnée</h2>", name)
                + detailRecipe + deleteRecipe + addIngredient + deleteIngredient;
    }

    @PostMapping("/detail")
    public static String detailRecipe(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        String name = params.get("name")[0];
        List<Recipe> recipes = Recipe.detailRecipe(name);
        StringBuilder html = new StringBuilder("<table border='2'><thead><tr><th>id</th><th>nom recette</th><th>description recette</th>" +
                "<th>Ingredient</th></tr></thead><tbdody>");
        if(recipes != null) {
            for (Recipe recipe: recipes) {
                StringBuilder nomIngredients = new StringBuilder("");
                StringBuilder qteIngredients = new StringBuilder("");
                StringBuilder measureUnit = new StringBuilder("");
                html.append(String.format("<tr><form action='http://127.0.0.1:8080/update' method='POST'>" +
                                "<td><input name='idr-%d' id='idr-%d' value='%d' readonly></td>" +
                                "<td><input name='namer-%d' id='namer-%d' value='%s'></td>" +
                                "<td><input name='descr-%d' id='descr-%d' value='%s'></td>",
//                                "<td><input name='descr-%s' id='descr-%s' value='%s'></td>" +
//                                "<td>%s</td>" +
//                                "<td>%s</td>" +
//                                "<td><button>Submit edit</button></td></form></tr>",
                        recipe.getId(), recipe.getId(), recipe.getId(),
                        recipe.getId(), recipe.getId(), recipe.getName(),
                        recipe.getId(), recipe.getId(),  recipe.getDescription()));
                html.append("<td><table border='1'><thead><tr><th>Id</th><th>Nom</th><th>Quantite</th><th>unite</th></tr></thead><tbody>");
                for(Ingredient ingredient: recipe.getIngredients()) {
                    html.append(String.format("<tr><td><input name='r-%d-i-%d' id='r-%d-i-%d' value='%d' readonly></td>",
                            recipe.getId(), ingredient.getId(), recipe.getId(), ingredient.getId(), ingredient.getId()));
                    html.append(String.format("<td><input name='namei-%d' id='namei-%d' value='%s'></td>",
                            ingredient.getId(), ingredient.getId(), ingredient.getName()));
                    html.append(String.format("<td><input name='qtei-%d' id='qtei-%d' value='%s'></td>",
                            ingredient.getId(), ingredient.getId(), ingredient.getQuantity()));
                    html.append(String.format("<td><input name='uniti-%d' id='uniti-%d' value='%s'></td>",
                            ingredient.getId(), ingredient.getId(), ingredient.getUnit()));
                    html.append("</tr>");
                }
                html.append("</table></td>");
                html.append("<td><button>Submit edit</button></td></form></tr>");
            }
        }
        html.append("</tbody></table>");
        return title + getAllRecipe + createRecipe + detailRecipe + resultat + html + deleteRecipe + addIngredient + deleteIngredient;
    }

    @PostMapping("/update")
    public static String updateRecipe(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        Set<String> keys = params.keySet();
        String ids = keys.stream().filter(x -> x.contains("idr-")).toList().toArray()[0].toString().replace("idr-", "");
        int id = Integer.parseInt(ids);
        String name = params.get("namer-" + id)[0];
        String description = params.get("descr-" + id)[0];
        Recipe recipe = new Recipe(id, name, description);
        List<String> ingredientsList = keys.stream().filter(x -> x.contains("r-" + id + "-i-")).toList();
        List<Ingredient> ingredients = new ArrayList<Ingredient>();
        for (String ingredient: ingredientsList) {
            int ingredientId = Integer.parseInt(ingredient.replace("r-" + id + "-i-", ""));
            if (ingredientId != 0) {
                String ingredientName = params.get("namei-" + ingredientId)[0];
                if (params.get("qtei-" + ingredientId)[0] == "") {
                    return "<h1>Values cannot be null</h1>";
                }
                int ingredientQuantity = Integer.parseInt(params.get("qtei-" + ingredientId)[0]);
                String ingredientUnit = params.get("uniti-" + ingredientId)[0];
                ingredients.add(new Ingredient(ingredientId, ingredientName, ingredientQuantity, ingredientUnit));
            }
        }
        Recipe.updateRecipe(recipe, ingredients);
        return title + getAllRecipe + createRecipe + detailRecipe + resultat + String.format("<h2>La recette: %s a bien été inseré dans la base de donnée</h2>", recipe.getName())
                + deleteRecipe + addIngredient + deleteIngredient;
    }

    @PostMapping("/deleterecipe")
    public static String deleteRecipe(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        if (Objects.equals(params.get("id")[0], "") || Objects.equals(params.get("id")[0], "0")) {
            return "<h1>Values cannot be null</h1>";
        }
        try {
            int id = Integer.parseInt(params.get("id")[0]);
            int result = Recipe.deleteRecipe(id);
            if (result == 0) {
                return title + getAllRecipe + createRecipe + detailRecipe + deleteRecipe + resultat + String.format("<h2>Ce numéro n'existe pas dans la bdd</h2>", id) + addIngredient + deleteIngredient;
            }
            return title + getAllRecipe + createRecipe + detailRecipe + deleteRecipe + resultat + String.format("<h2>La recette: %d a bien été supprimé de la base de donnée</h2>", id) + addIngredient + deleteIngredient;
        } catch (NumberFormatException e) {
            return "<h1>Values must be an int</h1>";
        }
    }
    @PostMapping("/addingredient")
    public static String addIngredient(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        if (Objects.equals(params.get("id")[0].trim(), "") || Objects.equals(params.get("id")[0], "0") ||
                Objects.equals(params.get("name")[0].trim(), "") || Objects.equals(params.get("quantity")[0].trim(), "") ||
                Objects.equals(params.get("name")[0], "0") || Objects.equals(params.get("unit")[0].trim(), "")) {
            return "<h1>Values cannot be null</h1>";
        }
        try {
            int id = Integer.parseInt(params.get("id")[0]);
            String name = params.get("name")[0];
            int quantity = Integer.parseInt(params.get("quantity")[0]);
            String unit = params.get("unit")[0];
            Ingredient ingredient = new Ingredient(0, name, quantity, unit);
            int result = Recipe.addIngredients(id, ingredient);
            if (result == 0) {
                return title + getAllRecipe + createRecipe + detailRecipe + deleteRecipe + resultat + addIngredient + String.format("<h2>Une erreur s'est produite</h2>", id) + deleteIngredient;
            }
            return title + getAllRecipe + createRecipe + detailRecipe + deleteRecipe + resultat  + addIngredient + "<h2>L'ingrédient a bien été rajouté à la recette</h2>" + deleteIngredient;
        } catch (NumberFormatException e) {
            return "<h1>Values must be an int</h1>";
        }
    }

    @PostMapping("/deleteingredient")
    public static String deleteIngredient(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        if (Objects.equals(params.get("idr")[0].trim(), "") || Objects.equals(params.get("idr")[0], "0") ||
                Objects.equals(params.get("idi")[0].trim(), "") || Objects.equals(params.get("idi")[0].trim(), "0"))  {
            return "<h1>Values cannot be null</h1>";
        }
        try {
            int idr = Integer.parseInt(params.get("idr")[0]);
            int idi = Integer.parseInt(params.get("idi")[0]);
            int result = Recipe.deleteIngredientFromRecipe(idr, idi);
            if (result == 0) {
                return title + getAllRecipe + createRecipe + detailRecipe + deleteRecipe + resultat + addIngredient
                        + deleteIngredient +  "<h2>Une erreur s'est produite</h2>" ;
            }
            return title + getAllRecipe + createRecipe + detailRecipe + deleteRecipe + resultat  + addIngredient
                    + deleteIngredient + "<h2>L'ingrédient a bien été supprimé de la recette</h2>";
        } catch (NumberFormatException e) {
            return "<h1>Values must be an int</h1>";
        }
    }
}
