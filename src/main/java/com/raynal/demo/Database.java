package com.raynal.demo;

import java.sql.*;

public class Database {
    public static Connection connection() {
        //prod
        String url = "jdbc:mysql://mysql_container:3306/recette";
        // local
//        String url = "jdbc:mysql://127.0.0.1:3306/recette";
        String username = "julien";
        String password = "julien";

        System.out.println("Connecting to database ...");

        try {
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot connect to the database");
        }
    }
}

