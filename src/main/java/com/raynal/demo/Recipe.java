package com.raynal.demo;

import com.fasterxml.jackson.databind.JsonSerializer;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


public class Recipe {
    final private int id;
    private String name;
    private String description;

    private ArrayList<Ingredient> ingredients;

    public Recipe(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Recipe(int id, String name, String description, ArrayList<Ingredient> ingredients) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ingredients = ingredients;
    }

    public static List<Recipe> get_all_recipe() {
        try (Connection connection = Database.connection()){
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM Recette");
            List<Recipe> objectList = new ArrayList<Recipe>();
            while (results.next()) {
                int id = results.getInt(1);
                String name = results.getString(2);
                String description = results.getString(3);
                objectList.add(new Recipe(id, name, description));
            }
            results.close();
            statement.close();
            connection.close();
            return objectList;
        } catch (SQLException e) {
            throw new IllegalStateException("Error in the request");
        }
    }

    public static void createRecipe(String name, String description) {
        try {
            if (name != null && !name.trim().isEmpty() && description != null && !description.trim().isEmpty()) {
                try (Connection connection = Database.connection()) {
                    Statement statement = connection.createStatement();
                    int results = statement.executeUpdate(String.format("INSERT INTO Recette (Nom, Description) VALUE ('%s', '%s')", name.toLowerCase(), description));
                    statement.close();
                    connection.close();
                } catch (SQLException e) {
                    throw new IllegalStateException("Error in the request");
                }
            } else {
                throw new Exception("empty name");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public static List<Recipe> detailRecipe(String partialName) {
        try {
            if (partialName != null && !partialName.trim().isEmpty()) {
                try (Connection connection = Database.connection()) {
                    Statement statement = connection.createStatement();
                    ResultSet results  = statement.executeQuery(String.format(
                            """
                                    SELECT Recette.Id, Recette.Nom, Recette.Description, I.Id, I.Nom, Q.quantite, Q.MeasureUnit
                                    FROM Recette
                                    LEFT JOIN Quantite Q on Recette.Id = Q.IdRecette
                                    LEFT JOIN Ingredient I on I.Id = Q.IdIngredient
                                    WHERE Recette.Nom like %s
                                    ORDER BY Recette.Id asc;""", "'%" +partialName.toLowerCase() + "%'")
                    );
                    List<Recipe> objectList = new ArrayList<Recipe>();
                    while (results.next()) {
                        if(objectList.size() > 0) {
                            Recipe origin = checkExistence(objectList, results.getInt(1));
                            if (origin != null) {
                                origin.ingredients.add(new Ingredient(
                                        results.getInt(4),
                                        results.getString(5),
                                        results.getInt(6),
                                        results.getString(7)
                                ));
                            } else {
                                addRecipe(results, (List<Recipe>) objectList);
                            }
                        } else {
                            addRecipe(results, (List<Recipe>) objectList);
                        }
                    }
                    statement.close();
                    connection.close();
                    return objectList;
                } catch (SQLException e) {
                    throw new IllegalStateException("Error in the request");
                }
            } else {
                throw new Exception("empty name");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public static void updateRecipe(Recipe recipe, List<Ingredient> ingredients) {
        try (Connection connection = Database.connection()){
            Statement statement = connection.createStatement();
            int results = statement.executeUpdate(String.format("""
                    UPDATE Recette
                    SET Nom = '%s', Description = '%s'
                    WHERE Id = %d;
                    """,
                    recipe.getName(), recipe.getDescription(), recipe.getId()));
            for (Ingredient ingredient: ingredients) {
                results = statement.executeUpdate(String.format("""
                        UPDATE Ingredient
                        SET Nom = '%s'
                        WHERE Id = %d;
                        """, ingredient.getName(), ingredient.getId()));
                results = statement.executeUpdate(String.format("""
                        UPDATE Quantite
                        SET quantite = %d, MeasureUnit = '%s'
                        WHERE IdRecette = %d AND IdIngredient = %d;
                        """,
                        ingredient.getQuantity(), ingredient.getUnit(),
                        recipe.getId(), ingredient.getId()
                        ));
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            throw new IllegalStateException("Error in the request");
        }
    }

    public static int deleteRecipe(int id) {
        try (Connection connection = Database.connection()){
            Statement statement = connection.createStatement();
            int results = statement.executeUpdate(String.format("""
                    DELETE
                    FROM Recette
                    WHERE Id = %d;
                            """,
                    id));
            statement.close();
            connection.close();
            return results;
        } catch (SQLException e) {
            throw new IllegalStateException("Error in the request");
        }
    }

    public static int addIngredients(int id, Ingredient ingredient) {
        try (Connection connection = Database.connection()){
            Statement checkStatement = connection.createStatement();
            ResultSet results  = checkStatement.executeQuery(String.format(
                    """
                    SELECT Id FROM Ingredient WHERE LOWER(Nom) like %s;
                    """, "'%" + ingredient.getName().toLowerCase() + "%'"
            ));
            int exisitingId = 0;
            while (results.next()) {
                exisitingId = results.getInt(1);
                break;
            }
            checkStatement.close();
            if (exisitingId == 0) {
                PreparedStatement pstatement = connection.prepareStatement(String.format("""
                                INSERT INTO Ingredient (Nom) VALUE ('%s');
                                        """,
                        ingredient.getName()), Statement.RETURN_GENERATED_KEYS);
                pstatement.execute();
                ResultSet rs = pstatement.getGeneratedKeys();
                rs.next();
                int result = rs.getInt(1);
                pstatement.close();
                if (result == 0) {
                    connection.close();
                    return result;
                }
                Statement statement = connection.createStatement();
                result = statement.executeUpdate(String.format("""
                        INSERT INTO Quantite (IdRecette, IdIngredient, quantite, MeasureUnit) VALUE (%d, %d, %d, '%s');
                        """, id, result, ingredient.getQuantity(), ingredient.getUnit()));
                statement.close();
                if (result == 0) {
                    connection.close();
                    return result;
                }
                statement.close();
                connection.close();
                return result;
            } else {
                Statement statement = connection.createStatement();
                int result = statement.executeUpdate(String.format("""
                        INSERT INTO Quantite (IdRecette, IdIngredient, quantite, MeasureUnit) VALUE (%d, %d, %d, '%s');
                        """, id, exisitingId, ingredient.getQuantity(), ingredient.getUnit()));
                statement.close();
                if (result == 0) {
                    connection.close();
                    return result;
                }
                statement.close();
                connection.close();
                return result;
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Error in the request");
        }
    }

    public static int deleteIngredientFromRecipe(int idr, int idi) {
        try (Connection connection = Database.connection()){
            Statement statement = connection.createStatement();
            int results = statement.executeUpdate(String.format("""
                    DELETE FROM Quantite WHERE IdRecette = %d AND IdIngredient = %d;
                            """,
                    idr, idi));
            statement.close();
            connection.close();
            return results;
        } catch (SQLException e) {
            throw new IllegalStateException("Error in the request");
        }
    }

    private static void addRecipe(ResultSet results, List<Recipe> objectList) throws SQLException {
        int id = results.getInt(1);
        String nom = results.getString(2);
        String description = results.getString(3);
        ArrayList<Ingredient> ingredient = new ArrayList<Ingredient>();
        ingredient.add(new Ingredient(
                results.getInt(4),
                results.getString(5),
                results.getInt(6),
                results.getString(7)
        ));
        objectList.add(new Recipe(id, nom, description, ingredient));
    }

    private static Recipe checkExistence(List<Recipe> recipes, int id) {
        for(Recipe recipe: recipes) {
            if(recipe.getId() == id) {
                return recipe;
            }
        }
        return null;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
