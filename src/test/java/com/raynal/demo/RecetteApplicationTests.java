package com.raynal.demo;

import org.apache.catalina.User;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.function.Try;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


class RecetteApplicationTests {
    @Test
    void testRecipeControl() {
        String recipeControl = Controller.recipeControl();
        assertThat(recipeControl).isEqualTo(Controller.basicPage);
    }

    @Test
    void testCreateRecipe() {
        // Allows to create a mock request to test the params (error is raised if param is missing or wrongly set)
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("name", "Test1");
        request.setParameter("description", "Test2");
        String createRecipe = Controller.createRecipe(request);
        assertThat(createRecipe).isEqualTo(Controller.title + Controller.getAllRecipe + Controller.createRecipe +
                Controller.resultat + String.format("<h2>La recette: %s a bien été inseré dans la base de donnée</h2>", "Test1")
                + Controller.detailRecipe + Controller.deleteRecipe + Controller.addIngredient + Controller.deleteIngredient);
    }

    @Test
    void testDetailRecipe() {
        // Allows to create a mock request to test the params (error is raised if param is missing or wrongly set)
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("name", "Test1");
        String detailRecipe = Controller.detailRecipe(request);
        assertThat(detailRecipe).isEqualTo(Controller.title + Controller.getAllRecipe + Controller.createRecipe
                + Controller.detailRecipe + Controller.resultat + "<table border='2'><thead><tr><th>id</th><th>nom recette</th><th>description recette</th>" +
                "<th>Ingredient</th></tr></thead><tbdody>" + "</tbody></table>"
                + Controller.deleteRecipe + Controller.addIngredient + Controller.deleteIngredient);
    }

    @Test()
    void testUpdateRecipe() {
        Throwable e = null;
        // Allows to create a mock request to test the params (error is raised if param is missing or wrongly set)
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("idr-1", "1");
        request.setParameter("namer-1", "Test1");
        request.setParameter("descr-1", "Desc1");
        request.setParameter("r-1-id-1", "Desc1");
        request.setParameter("namei-1", "Namei1");
        request.setParameter("qtei-1", "1");
        request.setParameter("uniti-1", "U1");
        try {
            Controller.updateRecipe(request);
        } catch (Throwable ex) {
            e = ex;
        }
        // Tests that the param are right but that at the end of all the parsing the db is just not accessible
        assertTrue(e instanceof IllegalStateException);
        assertThat(e).hasMessageContaining("Cannot connect to the database");
    }

    @Test
    void testDeleteRecipe() {
        Throwable e = null;
        // Allows to create a mock request to test the params (error is raised if param is missing or wrongly set)
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("id", "1");
        try {
            Controller.deleteRecipe(request);
        } catch (Throwable ex) {
            e = ex;
        }
        // Tests that the param are right but that at the end of all the parsing the db is just not accessible
        assertTrue(e instanceof IllegalStateException);
        assertThat(e).hasMessageContaining("Cannot connect to the database");
    }

    @Test
    void testAddIngredient() {
        Throwable e = null;
        // Allows to create a mock request to test the params (error is raised if param is missing or wrongly set)
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("id", "1");
        request.setParameter("name", "Test1");
        request.setParameter("quantity", "199");
        request.setParameter("unit", "cl");
        try {
            Controller.addIngredient(request);
        } catch (Throwable ex) {
            e = ex;
        }
        // Tests that the param are right but that at the end of all the parsing the db is just not accessible
        assertTrue(e instanceof IllegalStateException);
        assertThat(e).hasMessageContaining("Cannot connect to the database");
    }

    @Test
    void testDeleteIngredient() {
        Throwable e = null;
        // Allows to create a mock request to test the params (error is raised if param is missing or wrongly set)
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("idr", "1");
        request.setParameter("idi", "1");
        try {
            Controller.deleteIngredient(request);
        } catch (Throwable ex) {
            e = ex;
        }
        // Tests that the param are right but that at the end of all the parsing the db is just not accessible
        assertTrue(e instanceof IllegalStateException);
        assertThat(e).hasMessageContaining("Cannot connect to the database");
    }

}
